from gasp import *
import random

##########################################################

def place_player():
    global player,px,py
    px = random.randint(0,63)
    py = random.randint(0,47)

    player = Circle((10*px+5, py+5),5,filled=True)

def move_player():
    global player,px,py
    key = update_when('key_pressed')
    if key == "w" and py<47:
       py+=1
    move_to(player, (10*px+5,10*py+5))

##########################################################

begin_graphics()
finished = False

place_player()

while not finished:
    move_player()

end_graphics()

