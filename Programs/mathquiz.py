from random import randint

correct = 0

for i in range(10):
    num1 = randint(1,10)
    num2 = randint(1,10)
    question = f"What is {num1} times {num2}? "
    answer = num1 * num2
    response = int(input(question))

    if answer == response:
        print("That's right - well done.")
        correct += 1
    else:
        print(f"No, I'm afraid the answer is {answer}."

print(f"I asked you 10 questions. You got {correct} of them correct. Well done!")

